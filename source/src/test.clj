(prn "Hello")
(def root (babashka.fs/file
           (babashka.fs/expand-home "~/Sync/code/sites/blog-source/content")))
(def all-files (file-seq root))
(def markdown-files (filter (fn [f] (clojure.string/ends-with? (.getPath f) "md")) all-files))
(defn filename-no-ext [x] (str/join "" (drop-last 3 (.getName x))))
(def markdown (map (fn [f] {:file f :text (slurp f) :name (filename-no-ext f)}) markdown-files))
(defn matcher [text] (re-seq #"\(\{\{< relref \"(.*?)\" >\}\}\)" text))
(defn get-text [x] (get x :text))
(defn get-links [x] (get x :links))
(defn get-name [x] (get x :name))
(matcher (get (nth markdown 3) :text))
(def markdown-links (map (fn [x] (assoc x :links (matcher (get-text x)))) markdown))

(pr (map get-name markdown-links))

(def markdown-backlinks (map (fn [page] (assoc page :backlinks #{})) markdown-links))

;(pr markdown-backlinks)
(map (fn [page]

       (map (fn [links]

              (pr links)

              ) (get-links page))

       )

     markdown-links)